<?php
namespace CSVGenerator;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamWrapper;

class LSConfigurationINITest extends \PHPUnit_Framework_TestCase {
    public function testMissingRequiredFieldsParam() {
        $this->setExpectedException('Exception', 'Invalid ini configuration file, missing <fields>.');
        $this->obj->parse_conf();
    }
    
    protected function setUp() {
        vfsStream::setup('root');
        $this->setup_configuration_file();
        
        $this->obj = new LSConfigurationINI(vfsStream::url('root/no_fields_param.ini'));
    }
    
    private function setup_configuration_file() {
        $content = <<< "EOT"
; this is a comment
header = 1;
enclosure = "'";
delimiter = ";";
lines = 100;
EOT;
        vfsStream::newFile('no_fields_param.ini')
                ->withContent($content)
                ->at(vfsStreamWrapper::getRoot());
    }
}
