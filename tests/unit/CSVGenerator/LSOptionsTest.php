<?php
namespace CSVGenerator;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamWrapper;

/**
 * vfsStream is not working that well with SplInfo (and probably with SPLFileObjects)
 * that's why we're still using a classic fixture
 */

class LSOptionsTest extends \PHPUnit_Framework_TestCase {
   
    public function testInvalidExtensionConfigurationFile() {
       //vfsStream::newFile('example.ext')->at(vfsStreamWrapper::getRoot());
        $this->run_options(array('c' => vfsStream::url('root/example.ext'), 'o' => vfsStream::url('root/dummy.csv')));
    }
    
    public function testMissingOptionsCLIC() {
        $this->run_options(array('o' => '/tmp/dummy.csv'));
    }

    public function testMissingOptionsCLIO() {
        $this->run_options($options = array('c' => FIXTURES_DIR.'/simple.json'));
    }    
    
    public function testMissingOptionsCLIBoth() {
        $this->run_options(array('d' => ''));
    }    
    
    public function testInvalidConfigurationFileInvalidName() {
        //vfsStream::newFile('invalid_name.json')->at(vfsStreamWrapper::getRoot());
        $this->run_options(array('c' => vfsStream::url('root/invalid_name.json'), 'o' => 'dummy.csv'));
    }

    public function testInvalidConfigurationFileMissingConf() {
        $this->run_options(array('c' => '', 'o' => 'dummy.csv'));
    }    

    public function testInvalidConfigurationFileMissingOutput() {
        $this->run_options(array('c' => vfsStream::url('root/simple.json'), 'o' => ''));
    }        
    
    public function testJsonConfigurationOption() {
        $options = array('c' => FIXTURES_DIR.'/simple.json', 'o' => '/tmp/dummy.csv');

        $this->obj->parse($options);
        $conf = $this->obj->get_configuration();
        
        $this->assertInstanceOf('stdClass', $conf);
    }

    public function testINIConfigurationOption() {
        $options = array('c' => FIXTURES_DIR.'/simple.ini', 'o' => '/tmp/dummy.csv');

        $this->obj->parse($options);
        $conf = $this->obj->get_configuration();
        
        $this->assertInstanceOf('stdClass', $conf);
    }
    
    protected function setUp() {
        vfsStream::setup('root');
        $this->obj = new LSOptions;
    }
 
    protected function tearDown() {
        \Mockery::close();        
    }
    
    protected function run_options($options) {
        $this->setExpectedException('Exception');
        $this->obj->parse($options);
    }
}
