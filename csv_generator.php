#!/usr/bin/env php
<?php
#
# GENERATES CSV FILE BASED ON SPECIFIED CONF
#
#
#

require_once "vendor/autoload.php";

use CSVGenerator\LSOptions;
use CSVGenerator\LSFileGenerator;

$options = getopt("c:o:");

if (array_key_exists('h', $options) || !$options) {
    print_usage();
    exit(0);
}

// PARSING PARAMETERS
try {
    $ls_options = new LSOptions;
    $conf = $ls_options
            ->parse($options)
            ->get_configuration();

    $genObj = new LSFileGenerator($conf);
    $genObj->generate();
} catch (Exception $e) {
    echo $e->getMessage();
    exit(1);
}


// FUNCTIONS
function print_usage() {
    printf("Usage: %s -c <csv_configuration_file> -o <output_file> \n",basename(__FILE__));
    printf("-c <csv_configuration_file> - specify the configuration file; see example for syntax\n");
    printf("-o <output_file> - the output filename (with path if needed) \n");
}