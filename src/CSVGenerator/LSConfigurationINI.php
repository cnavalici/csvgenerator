<?php
namespace CSVGenerator;

/**
 * LSConfigurationINI
 *
 * @package: CSVGenerator
 * @author: Cristian NĂVĂLICI
 *
 */

use CSVGenerator\AbstractLSConfiguration;

class LSConfigurationINI extends AbstractLSConfiguration {
    static $fields_name_sep = "|";
    
    public function parse_conf() {
        $content = parse_ini_file($this->conf_filename, true);
        
        $this->extract_metadata_param("header", $content, 'intval');
        $this->extract_metadata_param("lines", $content, 'intval');        
        
        $this->extract_metadata_param("enclosure", $content);
        $this->extract_metadata_param("delimiter", $content);

        $this->extract_fields($content);
    }
    
    private function extract_metadata_param($param_name, $content, $cast = null) {
        if (array_key_exists($param_name, $content)) {
            $c = $content[$param_name];
            if ($cast) {
                $c = call_user_func($cast, $c);
            }
            
            $this->metadata->{$param_name} = $c;
        }
    }
    
    private function extract_fields($content) {
        if (array_key_exists('fields', $content)) {
            $fields_names = explode(self::$fields_name_sep, $content['fields']);
            
            $fdata = new \stdClass;
            foreach ($fields_names as $fn) {
                $fdata->{$fn} = (object)$content[$fn];
            }

            $this->fields_data = $fdata;
        } else {
            throw new \Exception("Invalid ini configuration file, missing <fields>.");
        }
        

    }
}
