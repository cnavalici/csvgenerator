<?php
namespace CSVGenerator;

/**
 * LSConfigurationJSON
 *
 * @package: CSVGenerator
 * @author: Cristian NĂVĂLICI
 *
 */

use CSVGenerator\AbstractLSConfiguration;

class LSConfigurationJson extends AbstractLSConfiguration {
    public function parse_conf() {
        $content = json_decode(file_get_contents($this->conf_filename));
        
        $this->extract_metadata_param("header", $content);
        $this->extract_metadata_param("enclosure", $content);
        $this->extract_metadata_param("delimiter", $content);
        $this->extract_metadata_param("lines", $content);
        
        $this->fields_data = $content->fields;
    }
    
    private function extract_metadata_param($param_name, $content) {
        if (isset($content->{$param_name})) {
            $this->metadata->{$param_name} = $content->{$param_name};
        }
    }
}
