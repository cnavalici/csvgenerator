<?php
namespace CSVGenerator;

/**
 * LSFieldFactory
 *
 * @package: CSVGenerator
 * @author: Cristian NĂVĂLICI
 *
 */

class LSFieldFactory {
    static private $workers = [];
    
    static public function create($conf) {
        if (!isset($conf->type)) {
            throw new \Exception("Required parameter 'type' is missing from the configuration.");
        }
        
        if (array_key_exists($conf->type, self::$workers)) {
            $worker = clone self::$workers[$conf->type];
            $worker->parse_configuration($conf);
            return $worker;
        }
    }

    static public function register($worker) {
        self::$workers[$worker::$wtype] = $worker;
    }
}

LSFieldFactory::register(new \CSVGenerator\LSFieldNumeric);
LSFieldFactory::register(new \CSVGenerator\LSFieldAlphaNumeric);
