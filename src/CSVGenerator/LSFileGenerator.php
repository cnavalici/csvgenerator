<?php

namespace CSVGenerator;

/**
 *
 * LSFileGenerator
 *
 * @package: CSVGenerator
 * @author: Cristian NĂVĂLICI
 *
 */
use CSVGenerator\LSLineGenerator;

class LSFileGenerator {

    private $csv_lines = [];
    private $bulk_execution = 10000;
    private $remaining_lines = 0;
    private $run_first_time_flag = true;
    private $output;
    private $metadata;
    private $fields_data;

    public function __construct(\stdClass $parsedOpt) {
        $this->output = $parsedOpt->output;
        $this->metadata = $parsedOpt->metadata;
        $this->fields_data = $parsedOpt->fields_data;
        $this->fields_names = $parsedOpt->fields_names;
    }

    public function generate() {
        $this->build_header();

        $this->remaining_lines = $this->metadata->lines;
        while ($this->remaining_lines >= 0) {
            $this->build_lines();
            $this->build_file();
            $this->remaining_lines -= $this->bulk_execution;
        }
    }

    private function build_header() {
        if ($this->metadata->header) {
            $this->csv_lines[] = (array) $this->fields_names;
        }
    }

    private function build_lines() {
        LSLineGenerator::prepare($this->fields_data);

        for ($ln = 0; $ln < $this->remaining_lines; $ln++) {
            $this->csv_lines[] = LSLineGenerator::build_line();
        }
    }

    private function build_file() {
        $fh = new \SplFileObject($this->output, 'a+');
        if ($this->run_first_time_flag) {
            $fh->ftruncate(0);
            $this->run_first_time_flag = false;
        }

        foreach ($this->csv_lines as $line) {
            $fh->fputcsv($line, $this->metadata->delimiter, $this->metadata->enclosure);
        }
    }

}
