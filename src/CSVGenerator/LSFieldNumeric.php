<?php
namespace CSVGenerator;

/**
 * LSFieldNumeric
 *
 * properties:
 * mandatory:
 *      type - numeric
 *      strategy - autoincrement | random
 * optional:
 *      start_at - initial value, default 0  
 *          min - (only for random)
 *          max - (only for random)
 * 
 * @package: CSVGenerator
 * @author: Cristian NĂVĂLICI
 *
 */

define('C_RAND', 'random');
define('C_AUTOINC', 'autoincrement');

class LSFieldNumeric extends AbstractLSFieldBase {
    static public $wtype = 'numeric';
    
    private $counter;
    private $accepted_strategies = array(C_RAND, C_AUTOINC);
    
    protected $min;
    protected $max;
    protected $strategy;
    protected $start_at;
    
    // this method also contains default values for different parameters
    public function parse_configuration(\stdClass $conf) {
        $strategy = $this->extract_configuration_param($conf, 'strategy', 'strtolower');

        if ($strategy && in_array($strategy, $this->accepted_strategies)) {
            $this->strategy = $strategy;
        } else {
            throw new \Exception("Invalid or missing strategy type ($strategy)");
        }
        
        $this->start_at = isset($conf->start_at) ? $conf->start_at : 1;
        $this->max = isset($conf->max) ? $conf->max : 10000;
        $this->min = isset($conf->min) ? $conf->min : 1;
        
        if ($this->strategy == C_AUTOINC) {
            $this->counter = $this->start_at;
        }
    }
    
    public function generate() {
        switch($this->strategy) {
            case C_AUTOINC:
                $field_value = $this->counter;
                $this->counter++;
            break;
            case C_RAND:
                $field_value = mt_rand($this->min, $this->max);
            break;
        }
        
        return $field_value;
    }
}