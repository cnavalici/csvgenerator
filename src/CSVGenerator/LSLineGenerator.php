<?php
namespace CSVGenerator;
/**
 *
 * LSLineGenerator
 *
 * @package: CSVGenerator
 * @author: Cristian NĂVĂLICI
 *
 */

class LSLineGenerator {
    static public $fields_obj;
    
    static public function prepare(\stdClass $fields_data) {
        foreach((array)$fields_data as $name => $conf) {
            self::$fields_obj[$name] = LSFieldFactory::create($conf);
        }
    }


    static public function build_line() {
        $built_line = array_map(function($obj) {
            return $obj->generate();
        }, self::$fields_obj);

        return $built_line;
    }
}
