<?php
namespace CSVGenerator;

/**
 * LSFieldBase
 *
 * @package: CSVGenerator
 * @author: Cristian NĂVĂLICI
 *
 */

abstract class AbstractLSFieldBase {
    static public $wtype = 'dummy';
    
    abstract public function generate();
    
    // include here different default values for parameters
    abstract public function parse_configuration(\stdClass $conf);
    
    protected function extract_configuration_param($conf, $param_name, $cast_func = null) {
        $param = null;
        
        if (isset($conf->{$param_name})) {
            $param = $conf->{$param_name};
            if ($cast_func) {
                $param = call_user_func($cast_func, $param);
            }
        }
        
        return $param;
    }
}