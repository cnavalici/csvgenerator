<?php
namespace CSVGenerator;
/**
 *
 * Base LSConfiguration
 *
 *
 * @package: CSVGenerator
 * @author: Cristian NĂVĂLICI
 *
 */


abstract class AbstractLSConfiguration {
    protected $conf_filename;
    
    protected $metadata; // general settings
    protected $fields_data;  // field specific settings

    public function __construct($conf_filename) {
        $fileinfo = new \SplFileInfo($conf_filename);
        
        if ($fileinfo->isReadable()) {
            $this->conf_filename = $conf_filename;
        } else {
            throw new \Exception("Cannot read configuration file: $conf_filename");
        }
        
        $this->populate_default_values();
    }    

    // these are the default values for broken configurations
    // they are the last safety net    
    private function populate_default_values() {
        $metadata = new \stdClass();
        $metadata->header = 1;
        $metadata->enclosure = "\"";
        $metadata->delimiter = ";";
        $metadata->lines = 10;
        
        $this->metadata = $metadata;
    }
    
    abstract public function parse_conf();
    
    // returns a stdClass configuration structure accepted by LSFileGenerator
    public function get_configuration() {
        $conf = new \stdClass();
        $conf->metadata = $this->metadata;
        $conf->fields_data = $this->fields_data;
        $conf->fields_names = (object)array_keys((array)$this->fields_data);
        return $conf;
    }
}

