<?php
namespace CSVGenerator;

/**
 * LSFieldAlphaNumeric
 *
 * properties:
 * mandatory:
 *      type - alphanumeric
 *      length - fixed size OR
 *      min_length / max_length - boundaries for variable length
 * optional:
 *      only_alpha: true|false      
 * 
 * @package: CSVGenerator
 * @author: Cristian NĂVĂLICI
 *
 */

class LSFieldAlphaNumeric extends AbstractLSFieldBase {
    static public $wtype = 'alphanumeric';
    
    private $length;
    private $min_length;
    private $max_length;
    
    private $pool;
    private $pool_size;

    // this method also contains default values for different parameters
    public function parse_configuration(\stdClass $conf) {
        $this->length = $this->extract_configuration_param($conf, 'length', 'intval');
        $this->min_length = $this->extract_configuration_param($conf, 'min-length', 'intval');
        $this->max_length = $this->extract_configuration_param($conf, 'max-length', 'intval');
        
        if (!$this->length && (!$this->min_length || !$this->max_length)) {
            throw new \Exception("Length configuration parameter is missing.");
        }
        
        $alpha_flag = $this->extract_configuration_param($conf, 'only-alpha');
        if ($alpha_flag) {
            $this->pool = array_merge(range('a', 'z'), range('A', 'Z'));
        } else {
            $this->pool = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
        }
        
        $extra_chars = $this->extract_configuration_param($conf, 'extra-chars');
        if ($extra_chars) {
            $this->pool = array_merge($this->pool, str_split($extra_chars));
        }

        $this->pool_size = count($this->pool);        
    }    
    
    public function generate() {
        $field_value = '';

        $length = $this->length ? $this->length : mt_rand($this->min_length, $this->max_length);
    
        for ($k = 0; $k < $length; $k++) {
            $field_value .= $this->pool[mt_rand(0, $this->pool_size - 1)];
        }
        
        return $field_value;
    }
}