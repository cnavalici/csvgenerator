<?php
namespace CSVGenerator;
/**
 *
 * LSOptions
 *
 * parse the options from command line and produce a fully configuration object
 * 
 * @package: CSVGenerator
 * @author: Cristian NĂVĂLICI
 *
 */

use CSVGenerator\LSConfigurationJson;
use CSVGenerator\LSConfigurationINI;
//use CSVGenerator\LSConfigurationXML; // for future jobs

class LSOptions {
    protected $configuration;

    public function parse($options) {
        if (array_key_exists('c', $options)) {
            $this->read_configuration($options['c']);
        } else {
            throw new \Exception("You must specify a valid configuration filename");
        }

        if (array_key_exists('o', $options) && trim($options['o'])) {
            $this->configuration->output = $options['o'];
        } else {
            throw new \Exception("You must specify a valid output filename");
        }
        
        return $this;
    }


    protected function read_configuration($conf_filename = '') {
        $conf = $this->manufacture_configuration($conf_filename);
        $conf->parse_conf();
        
        $this->configuration = $conf->get_configuration();
    }

    
    public function get_configuration() {
        return $this->configuration;
    }
    
    
    protected function manufacture_configuration($conf_filename) {
        $fileinfo = new \SplFileInfo($conf_filename);
        $extension = $fileinfo->getExtension();
        
        switch($extension) {
            case 'json':
                $conf = new LSConfigurationJson($conf_filename);
            break;
            case 'ini':
                $conf = new LSConfigurationINI($conf_filename);
            break;
            /*case 'xml':
                $conf = new LSConfigurationINI($conf_filename);
            break; */
            default:
                throw new \Exception("Unknown configuration file type.");
        }
        
        return $conf;
    }
}