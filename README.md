# CSV Generator



## What is this?

CSV Generator is a tool written in PHP, useful in the scenario when you need a test CSV file with dummy data.
It's capable to generate huge files, so a three millions lines is just a matter of personal choice :)


## Installation


Unpack/copy the package at a convenient location and run it as: <br />

    php csv_generator.php -c <path_to_configuration_file> -o <output_file.csv>
    
or if you made it executable:

    chmod +x csv_generator.php
    ./csv_generator.php -c <path_to_configuration_file> -o <output_file.csv>

Make sure you supply both parameters, as they are mandatory.


## Configuration files

### General structure

Generating CSV is based on some kind of template, where you specify how the columns should look like,
how big will be the file, etc. This is done into configuration files, following a certain structure.

If you're eager to jump into it, just check the configuration files under _data_ directory, as _example.ini_
and _example.json_. For now, these are the accepted input formats.

Mandatory elements for the configuration files:
    
__example.ini__

    ; this is a comment
    header = 1;
    enclosure = '"';
    delimiter = "|";
    lines = 123;

    ; the list with the fields names separated by |
    fields = "id|name"

    [id]
    ...

    [name]
    ...

The same example, translated to json format:

__example.json__

    {
        "header": 1,
        "enclosure": "\"",
        "delimiter": "|",
        "lines": 123,
        "fields": {
            "id" : {
                ...
            },
            "name": {
                ...
            }
        }
    }


### Configuration structure explained

Decides if the first line of the file will contain the field names or not.
    
    header = 1|0

Field enclosure for generated CSV.

    enclosure = <char>

Separator for fields.

    delimiter = <char>

How many lines will be generated (plus the header if any).

    lines = <integer>

Check next the accepted fields configurations.
    fields = these are with different configuration based on their type.



### Fields configuration

#### Numeric

    mandatory:
        type - numeric
        strategy - autoincrement | random
    optional:
        start_at - initial value, default 0  
        min - (only for random)
        max - (only for random)


#### Alphanumeric / Alpha only

 
    mandatory:
        type - alphanumeric
        length - fixed size OR
        min-length / max-length - boundaries for variable length
    optional:
        only-alpha: true|false 
        extra-chars: <string with extra special chars>

### Examples

I'll be back...


## Credits

Author: Cristan Năvălici <br />
Timestamp: march 2013
Updated: may 2014
